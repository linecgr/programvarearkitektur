package com.wetidiots.game;

import android.util.Log;

import androidx.annotation.NonNull;

import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.wetidiots.game.models.GamePlay;
import com.wetidiots.game.models.QuestionGenerator;
import com.wetidiots.game.models.User;
import com.wetidiots.game.views.JoinView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static android.content.ContentValues.TAG;

public class AndroidInterfaceClass implements FirebaseInterface{

    private FirebaseFirestore databaseFirestore;
    private FirebaseDatabase databaseRTD;
    private DatabaseReference databaseReference;
    private FirebaseAuth mAuth;

    private List data = new ArrayList<>();
    private String errorMessage = "";
    private String username="";
    private boolean unused=true;

    public AndroidInterfaceClass(){
        databaseFirestore = FirebaseFirestore.getInstance();
        databaseRTD = FirebaseDatabase.getInstance("https://wetidiots-default-rtdb.europe-west1.firebasedatabase.app/");
        databaseReference = databaseRTD.getReference();
        mAuth = FirebaseAuth.getInstance();
    }

    // Methods for firestore below

    // Creates a user account in Firebase with the given email and password.
    // Writes the user's email, password and username to Firestore
    @Override
    synchronized public void createUser(String email, String password, String userName) {
        if(checkUserName(userName)) {
            mAuth.createUserWithEmailAndPassword(email, password)
                    .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                System.out.println("createUser: success!!");
                                String userId = mAuth.getCurrentUser().getUid();
                                Map<String, Object> user = new HashMap<>();
                                user.put("username", userName);
                                user.put("email", email);
                                user.put("score", 0);
                                databaseFirestore.collection("users")
                                        .document(userId).set(user);
                                databaseFirestore.collection("username").document("name").update("name", FieldValue.arrayUnion(userName));
                            } else {
                                System.out.println("createUser: failure" + task.getException());
                                errorMessage = "Authentication failed. The email is used by another account.";
                            }
                        }});
        } else {
            System.out.println("Username taken");
            errorMessage="Username taken";
        }
    }

    // Check if a username is already in use
    private boolean checkUserName(String userName){
        this.unused=true;
        databaseFirestore.collection("username").document("name").get().addOnCompleteListener(task->{
            if(task.isSuccessful()){
                Object name=task.getResult().get("name");
                ArrayList names = (ArrayList) name;
                for(Object n: names){
                    if(n.toString().equals(userName)){
                        this.unused=false;
                    }
                }
            }
        });
        try{
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return this.unused;
    }

    // Checks if the user is signed in
    @Override
    public boolean isSignedIn() {
        return mAuth.getCurrentUser()!= null;
    }

    // Find username of given email
    @Override
    public String getUsername(String email){
        databaseFirestore.collection("users").whereEqualTo("email", email).get().addOnCompleteListener(task->{
            if(task.isSuccessful()){
                QuerySnapshot document=task.getResult();
                for (QueryDocumentSnapshot q: document){
                    this.username=q.get("username").toString();
                }
            }
        });
        return this.username;
    }

    // Sign in the user with the given email and password
    @Override
    public void logIn(String email, String password) {
        mAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if(task.isSuccessful()){
                    System.out.println("login: success!!");
                } else {
                    System.out.println("login: failure" + task.getException());
                    errorMessage = "Authentication failed. Please check your email and password.";
                }
            }
        });
    }

    // Gets userId of current user, sets username and email of given user and adds userId and username to list in User
    @Override
    public void getUid(List userId, User user){
        userId.clear();
        String uid = mAuth.getCurrentUser().getUid();
        userId.add(uid);
        databaseFirestore.collection("users/").document(uid).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                DocumentSnapshot doc = task.getResult();
                if (user.getUserName() == null){
                    user.setUserName(doc.get("username").toString());
                    user.setEmail(doc.get("email").toString());
                    user.setId(uid);
                }
                if (task.isSuccessful() && userId.size() < 2){
                    userId.add(1, doc.get("username").toString());
                }
            }
        });
    }

    // Updates the user's score in Firestore (adds the new score to the user's total score).
    @Override
    public void updateGlobalScore(int newScore) {
        String userId = mAuth.getCurrentUser().getUid();
        DocumentReference ref = databaseFirestore.collection("users").document(userId);
        ref.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if(task.isSuccessful()){
                    DocumentSnapshot document = task.getResult();
                    int oldScore = Integer.parseInt(document.get("score").toString());
                    ref.update("score",oldScore + newScore);

                }
            }
        });
    }

    @Override
    public String getErrorMessage() {
        return errorMessage;
    }

    @Override
    public void signOut(){
        mAuth.signOut();
    }

    // Reads a question at given index from firestore and set it as nextQuestion in QuestionGenerator
    @Override
    public void readQuestion(String index, QuestionGenerator questionGenerator){
        List<String> questions = new ArrayList<>();
        databaseFirestore.document("questions/"+index)
                .get()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        DocumentSnapshot document = task.getResult();
                        document.getData().values().stream().forEach(v -> questions.add(v.toString()));
                        questionGenerator.setNextQuestion(questions);
                    } else {
                        Log.w(TAG, "Error getting documents.", task.getException());
                    }
                });
    }

    // Sorts collection in Firestore based on the value in score field and adds the 10 highest score to dataLocation
    @Override
    public void orderValuesFromFirestore(String collection, List dataLocation) {
        databaseFirestore.collection(collection)
                .orderBy("score", Query.Direction.DESCENDING).limit(10)
                .get()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        for (QueryDocumentSnapshot document : task.getResult()) {
                            dataLocation.add(document.getData());
                        }
                    } else {
                        Log.w(TAG, "Error getting documents.", task.getException());
                    }
                });
    }

    // Methods for realtime database below

    // Add value to Realtime Database
    @Override
    public void addValuesToRTD(String target, Object value) {
        databaseReference = databaseRTD.getReference(target);
        databaseReference.setValue(value);
    }

    // Check if gamePin already exist and if not, create a game and add to Realtime Database
    @Override
    public void createGame(GamePlay game){
        databaseReference = databaseRTD.getReference("games");
        databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                List games = new ArrayList();
                // creates list with gamePins from already existing games
                snapshot.getChildren().forEach(game -> games.add(game.getKey().toString()));
                // checks if gamePin already exists
                if ((!games.contains(game.getGamePin())) && game.getGamePin().length()>1){
                    game.getPlayers().add(game.getPlayer1());
                    addValuesToRTD("games/"+game.getGamePin(), game);
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Log.d(TAG, "Failed", error.toException());
            }
        });
    }

    // Delete a Gameplay from database
    @Override
    public void deleteGame(GamePlay game){
        databaseRTD.getReference("games").child(game.getGamePin()).removeValue();
    }

    // Delete player at playerNumber from game given by gamePin
    @Override
    public void deletePlayer(int playerNumber, String gamePin){
        databaseRTD.getReference("games").child(gamePin).child("players").child(""+playerNumber).removeValue();
    }

    // Joins game with gamePin if game exist in Realtime Database and players size is less than 4
    @Override
    public void joinGame(String gamePin, HashMap player, JoinView view){
        ArrayList players = new ArrayList();
        databaseReference = databaseRTD.getReference("games").child(gamePin);
        databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                snapshot.child("players").getChildren().forEach(p ->players.add(p));
                if (snapshot.exists() && players.size()<4){
                    Map<String, Object> childUpdates = new HashMap<>();
                    childUpdates.put("players/" +players.size(), player);
                    databaseReference.updateChildren(childUpdates);
                    view.errorMessage = "";
                    view.joined = "Waiting for game to start...";
                    view.join_btn.setTouchable(Touchable.disabled);
                }
                else{
                    if (players.size()>=4){
                        view.errorMessage = "Lobby is full";
                    }
                    else{
                        view.errorMessage = "GamePin does not exist";
                    }
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Log.d(TAG, "Failed", error.toException());
            }
        });
    }

    @Override
    public void isGameActive(GamePlay game, String gamePin){
        System.out.println("gamepin ->" + gamePin);
        databaseReference = databaseRTD.getReference("games").child(gamePin).child("ready");
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()){
                    boolean value = (boolean) snapshot.getValue();
                    game.setReady(value);
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Log.d(TAG, "Failed", error.toException());
            }
        });
    }

    // Sets ready value of a game
    @Override
    public void setReady(String gamePin, boolean ready){
        databaseRTD.getReference("games").child(gamePin).child("ready").setValue(ready);
    }

    // Gets value of ready to given game from Realtime Database and updates local Gameplay with value from RD
    @Override
    public void getReady(GamePlay game){
        databaseReference = databaseRTD.getReference("games").child(game.getGamePin()).child("ready");
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()){
                    game.setReady((Boolean) snapshot.getValue());
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Log.d(TAG, "Failed", error.toException());
            }
        });
    }

    // Gets players in given Gameplay and username of players to a list
    @Override
    public void getPlayers(GamePlay game, List usernames){
        ArrayList<HashMap> players = new ArrayList();
        databaseReference = databaseRTD.getReference("games").child(game.getGamePin()).child("players");
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                snapshot.getChildren().forEach(p -> players.add((HashMap) p.getValue()));
                for (HashMap p: players){
                    if (!usernames.contains(p.get("username"))){
                        usernames.add(p.get("username"));
                    }
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Log.d(TAG, "Failed", error.toException());
            }
        });
    }

    // Update player in Realtime Database game with given playerNumber
    @Override
    public void updatePlayer(GamePlay game, int playerNumber){
        databaseRTD.getReference("games")
                .child(game.getGamePin())
                .child("players")
                .child(((Integer) playerNumber).toString())
                .setValue(game.getPlayers().get(playerNumber));
    }

    // Reads values in Gameplay to correct game in Realtime Database
    int count = 0;
    @Override
    public void readToGamePlay(GamePlay game){
        databaseReference = databaseRTD.getReference("games").child(game.getGamePin());
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()){
                    HashMap<String, Object> value = new HashMap<>();
                    snapshot.getChildren().forEach(child -> value.put(child.getKey(), child.getValue()));
                    game.updateGamePlay(value);
                    count++;
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Log.d(TAG, "Failed", error.toException());
            }
        });
    }

    // Update currentQ and nextQ in given game in Realtime Database
    @Override
    public void updateQuestions(GamePlay game, List<String> currentQ, List<String> nextQ){
        databaseRTD.getReference("games")
                .child(game.getGamePin())
                .child("currentQ")
                .setValue(currentQ);
        databaseRTD.getReference("games")
                .child(game.getGamePin())
                .child("nextQ")
                .setValue(nextQ);
    }

    // Read currentQ and nextQ from game in Realtime Database and update local Gameplay game
    @Override
    public void readQuestions(GamePlay game){
        databaseReference = databaseRTD.getReference("games").child(game.getGamePin());
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()){
                    game.setCurrentQ((List) snapshot.child("currentQ").getValue());
                    game.setNextQ((List) snapshot.child("nextQ").getValue());
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Log.d(TAG, "Failed", error.toException());
            }
        });
    }

    // Update score of player with given playerNumber in Realtime Database with given score
    @Override
    public void updateScore(GamePlay game, int score, int playerNumber) {
        databaseRTD.getReference("games")
                .child(game.getGamePin())
                .child("players")
                .child(((Integer) playerNumber).toString())
                .child(((Integer) score).toString())
                .setValue(game.getPlayers().get(playerNumber).get(score));
    }
}