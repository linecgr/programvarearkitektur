package com.wetidiots.game.views;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.wetidiots.game.Game;

// Local Leaderboard is available when all players of a game is dead
public class LocalLeaderboardView extends View {

    private Rectangle bounds;
    private BitmapFont font;
    private String header;
    private Texture water;
    private ImageButton back_btn;
    private Stage stage;
    private Skin skin;
    private String username;

    protected LocalLeaderboardView(GameStateManager gsm) {
        super(gsm);
        StretchViewport viewport = new StretchViewport(Game.WIDTH,Game.HEIGHT,cam);
        stage = new Stage(viewport);
        Gdx.input.setInputProcessor(stage);
        skin = new Skin(Gdx.files.internal("skin/flat-earth-ui.json"));

        back_btn = new ImageButton(skin);
        back_btn.getStyle().imageUp = new TextureRegionDrawable(new TextureRegion(new Texture("back.png")));
        back_btn.setWidth(60);
        back_btn.setHeight(60);
        back_btn.setPosition(0,Game.HEIGHT-back_btn.getHeight());
        addListeners();
        stage.addActor(back_btn);

        Gdx.gl.glClearColor(1,1, 1,1);
        water = new Texture("water1.png");
        bounds = new Rectangle(12*adjust,565*adjust+4, 35*adjust, 35*adjust);
        font = new BitmapFont(Gdx.files.internal("Unnamed.fnt"));
        font.getData().setScale(0.30f,0.30f);
        header = "Scoreboard from Game";
        username = Game.userController.getUser().getUserName();
        System.out.println("Leaderboard, ME----> " + username);
        updateScore();
    }

    private void addListeners(){
        back_btn.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y){
                if(username.equals(Game.gameController.getUsernamePlayer(0))){ // when a creator hits back, the game should be deleted from the database
                    Game.gameController.deleteGame();
                }
                gsm.set(new MainMenuView(gsm));
                Game.gameController.clearGameController(); // clear local gameController+games of all players if they go back from localLeaderboard
            }
        });
    }

    // Update your own score and add to your global score
    public void updateScore(){
        int selfPosition = Game.gameController.getGame().findCorrectHashMap(username);
        Game.leaderboardController.updateGlobalScore(Game.gameController.getPlayersScore(selfPosition));
    }

    @Override
    public void handleInput() {
    }

    @Override
    public void update(float dt) {
        cam.update();
    }

    @Override
    public void render(SpriteBatch sb) {
        sb.setProjectionMatrix(cam.combined);
        sb.begin();
        stage.draw();
        sb.draw(water, 0, 0, 360*adjust, 236*adjust);
        font.draw(sb, header, 50*adjust, 550*adjust);
        // Draw the local leaderboard / result list
        if (Game.gameController.getGameScore().size() >= 1){
            font.draw(sb, "1", 5*adjust, 480*adjust);
            font.draw(sb, Game.gameController.getGameScore().get(0).toString(), 50*adjust, 480*adjust);
            if(Game.gameController.getGameScore().size()>=2){
                font.draw(sb, "2", 5*adjust, 460*adjust);
                font.draw(sb, Game.gameController.getGameScore().get(1).toString(), 50*adjust, 460*adjust);
            }
            if(Game.gameController.getGameScore().size()>=3){
                font.draw(sb, "3", 5*adjust, 440*adjust);
                font.draw(sb, Game.gameController.getGameScore().get(2).toString(),50*adjust, 440*adjust);
            }
            if(Game.gameController.getGameScore().size()>=4){
                font.draw(sb, "4", 5*adjust, 420*adjust);
                font.draw(sb, Game.gameController.getGameScore().get(3).toString(), 50*adjust, 420*adjust);
            }
        }
        sb.end();
    }

    @Override
    public void dispose() {
        stage.dispose();
        water.dispose();
        font.dispose();
    }
}



