package com.wetidiots.game.views;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Dialog;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

public class Message {
    private Skin skin;

    public Message( ) {
        skin = new Skin(Gdx.files.internal("skin/flat-earth-ui.json"));
    }

    /**
     * Creates a dialog with the provided errormessage
     * @param errorMessage The errormessage to be displayed to the user
     * @return dialog with errormessage
     */
    public Dialog generateErrorMessage(String errorMessage){
        final Dialog dialog = new Dialog("Warning", skin);
        TextButton okBtn = new TextButton("Ok", skin);
        //okBtn.setColor(0.32f, 0.61f, 0.21f, 1);
        okBtn.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y){
                dialog.hide();
                Gdx.gl.glClearColor(1,1, 1,1);
            }
        });
        Table table = new Table(skin);
        Label errorLabel = new Label(errorMessage, skin);
        errorLabel.setWrap(true);
        table.add(errorLabel).width(290).padBottom(10);
        table.row();
        table.add(okBtn).padBottom(10);
        dialog.add(table);
        return dialog;
    }
    /**
     * Creates a dialog with the provided infomessage
     * @param infoMessage The infomessage to be displayed to the user
     * @return dialog with infomessage
     */
    public Dialog generateInfoMessage(String infoMessage){
        final Dialog dialog = new Dialog("Information", skin);
        Table table = new Table(skin);
        Label infoLabel = new Label(infoMessage, skin);
        infoLabel.setWrap(true);
        table.add(infoLabel).width(290).padBottom(10);
        dialog.add(table);
        return dialog;
    }
}
