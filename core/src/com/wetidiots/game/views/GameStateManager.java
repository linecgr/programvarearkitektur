package com.wetidiots.game.views;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import java.util.Stack;

// To handle changes in views
public class GameStateManager {
    private static GameStateManager gsm = new GameStateManager(); // Singleton pattern

    private Stack<View> views;

    private GameStateManager() {
        views = new Stack<>();
    } // Singleton pattern

    public static GameStateManager getGsm(){ // Singleton pattern
        return gsm;
    } // Singleton pattern

    public void push(View s){
        views.push(s);
    }

    public void set(View s){
        views.pop().dispose();
        views.push(s);
    }

    public void update(float dt){
        views.peek().update(dt);
    }

    public void render(SpriteBatch sb){
        views.peek().render(sb);
    }
}
