package com.wetidiots.game.views;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.wetidiots.game.Game;
import com.wetidiots.game.controllers.UserController;

public class SettingsView extends View {
    private UserController userController;

    private Texture logo;
    private Texture water;
    private Texture sound;
    private Texture onswitch;
    private Texture offswitch;
    private Rectangle soundswitch_bounds;
    private BitmapFont font;
    private String text1;
    private String text2;

    private Stage stage;
    private Skin skin;

    private TextButton signOutBtn;
    private ImageButton back_btn;

    public SettingsView(GameStateManager gsm) {
        super(gsm);
        this.userController=Game.userController;
        Gdx.gl.glClearColor(1,1, 1,1);
        StretchViewport viewport = new StretchViewport(Game.WIDTH,Game.HEIGHT,cam);
        stage = new Stage(viewport);
        Gdx.input.setInputProcessor(stage);
        skin = new Skin(Gdx.files.internal("skin/flat-earth-ui.json"));

        logo = new Texture("settingslogo.png");
        water = new Texture("water1.png");
        sound = new Texture("sound.png");
        onswitch = new Texture("onswitch.png");
        offswitch = new Texture("offswitch.png");
        soundswitch_bounds = new Rectangle(670, 780, 120, 70);

        signOutBtn = new TextButton("Sign out", skin);
        signOutBtn.setSize(140,60);
        signOutBtn.setPosition(Game.WIDTH-160,900);

        font = new BitmapFont(Gdx.files.internal("Unnamed.fnt"));
        font.getData().setScale(0.30f,0.30f);
        text1 = "on";
        text2 = "off";

        back_btn = new ImageButton(skin);
        back_btn.getStyle().imageUp = new TextureRegionDrawable(new TextureRegion(new Texture("back.png")));
        back_btn.setSize(90,90);
        back_btn.setPosition(0,Game.HEIGHT-back_btn.getHeight());
        addListeners();
        stage.addActor(back_btn);
        stage.addActor(signOutBtn);
    }

    private boolean inBounds(float x, float y, Rectangle bounds){
        return bounds.contains(x, y);
    }

    private void addListeners(){
        back_btn.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y){
                gsm.set(new MainMenuView(gsm));
            }
        });
        signOutBtn.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y){
                userController.signOut();
                gsm.set(new StartScreenView(gsm));
            }
        });
    }

    @Override
    public void handleInput() {
        if (Gdx.input.justTouched() && inBounds(Gdx.input.getX(), Gdx.input.getY(), soundswitch_bounds)){
            Game.musicmodel.musicClicked(); //Changes the music object
            if (Game.musicmodel.getMusic()){
                Game.music.play();
            } else {
                Game.music.stop();
            }
        }
    }

    @Override
    public void update(float dt) {
        handleInput();
        cam.update();
    }

    @Override
    public void render(SpriteBatch sb) {
        sb.setProjectionMatrix(cam.combined);
        sb.begin();
        stage.draw();
        sb.draw(logo, 80*adjust, 520*adjust, 194*adjust,70*adjust);
        sb.draw(water, 0, 0, Game.WIDTH, Game.HEIGHT/2);
        sb.draw(sound, Game.WIDTH/2-120, 550, 70, 70);
        // Button changes depending on if the music is playing or not
        if (Game.musicmodel.getMusic()){
            sb.draw(onswitch, Game.WIDTH/2+50, 550, 70, 70);
            font.draw(sb, text1, Game.WIDTH/2+70, 540);
        } else {
            sb.draw(offswitch, Game.WIDTH/2+50, 550, 70, 70);
            font.draw(sb, text2, Game.WIDTH/2+70, 540);
        }
        sb.end();
    }

    @Override
    public void dispose() {
        logo.dispose();
        water.dispose();
        sound.dispose();
        stage.dispose();
        font.dispose();
        onswitch.dispose();
        offswitch.dispose();
    }
}
