package com.wetidiots.game.views;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.wetidiots.game.Game;

public class StartScreenView extends View{
    private Stage stage;
    private Skin skin;
    private Texture logo;
    private OrthographicCamera camera;

    private TextButton login;
    private TextButton register;

    public StartScreenView(final GameStateManager gsm) {
        super(gsm);
        camera = new OrthographicCamera();
        camera.setToOrtho(false,Game.WIDTH, Game.HEIGHT);
        StretchViewport viewp = new StretchViewport(Game.WIDTH, Game.HEIGHT, camera);
        Gdx.gl.glClearColor(1,1, 1,1);

        stage = new Stage(viewp);
        Gdx.input.setInputProcessor(stage);
        skin = new Skin(Gdx.files.internal("skin/flat-earth-ui.json"));

        logo = new Texture("logo.png");

        register = new TextButton("Register", skin);
        register.setSize(170,70);
        register.setPosition(Game.WIDTH/2- register.getWidth()/2,295);

        login = new TextButton("Login", skin);
        login.setSize(170,70);
        login.setPosition(Game.WIDTH/2- register.getWidth()/2,395);

        addListeners();
        stage.addActor(login);
        stage.addActor(register);
    }

    private void addListeners(){
        login.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y){
                gsm.set(new LogInView(gsm));
            }});
        register.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y){
                gsm.set(new RegisterView(gsm));
            }});
    }

    @Override
    public void handleInput() {
    }

    @Override
    public void update(float dt) {
    }

    @Override
    public void render(SpriteBatch sb) {
        sb.setProjectionMatrix(camera.combined);
        sb.begin();
        sb.draw(logo, Game.WIDTH/2-200, 500, 400,160);
        sb.end();
        stage.act();
        stage.draw();

    }

    @Override
    public void dispose() {
        stage.dispose();
        logo.dispose();
    }
}
