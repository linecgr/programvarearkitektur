package com.wetidiots.game.views;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.wetidiots.game.Game;

public class TutorialView extends View {
    private ImageButton back_btn;
    private Texture pic1;
    private Stage stage;
    private Skin skin;
    private Label header;

    protected TutorialView(GameStateManager gsm) {
        super(gsm);
        cam.setToOrtho(false, Game.WIDTH, Game.HEIGHT);
        Gdx.gl.glClearColor(1,1, 1,1);
        StretchViewport viewport = new StretchViewport(Game.WIDTH,Game.HEIGHT,cam);
        stage = new Stage(viewport);
        Gdx.input.setInputProcessor(stage);
        skin = new Skin(Gdx.files.internal("skin/flat-earth-ui.json"));

        back_btn = new ImageButton(skin);
        back_btn.getStyle().imageUp = new TextureRegionDrawable(new TextureRegion(new Texture("back.png")));
        back_btn.setWidth(60);
        back_btn.setHeight(60);
        back_btn.setPosition(0,Game.HEIGHT-back_btn.getHeight());
        addListeners();
        header = new Label("Tutorial", skin, "title");
        header.setWidth(Game.WIDTH);
        header.setHeight(50);
        header.setAlignment(Align.center);
        header.setPosition(0, 850);
        pic1 = new Texture("tutorial.png");
        stage.addActor(back_btn);
        stage.addActor(header);
    }

    @Override
    public void handleInput() {
    }

    private void addListeners(){
        back_btn.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y){
                Game.gameController.clearGameController();
                gsm.set(new MainMenuView(gsm));
            }
        });
    }
    @Override
    public void update(float dt) {
        handleInput();
    }

    @Override
    public void render(SpriteBatch sb) {
        sb.setProjectionMatrix(cam.combined);
        sb.begin();
        stage.draw();
        sb.draw(pic1, 3, 100, pic1.getWidth()-230, pic1.getHeight());
        sb.end();
    }

    @Override
    public void dispose() {
        stage.dispose();
        pic1.dispose();
    }
}
