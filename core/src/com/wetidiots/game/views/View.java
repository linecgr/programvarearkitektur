package com.wetidiots.game.views;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public abstract class View {
    protected GameStateManager gsm;
    protected OrthographicCamera cam;
    protected float adjust = 800/600f;


    public View(GameStateManager gsm){
        this.gsm = gsm;
        this.cam = new OrthographicCamera();
    }

    public abstract void handleInput();
    public abstract void update(float dt);
    public abstract void render(SpriteBatch sb);
    public abstract void dispose();
}
