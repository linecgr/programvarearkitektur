package com.wetidiots.game.views;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.TimeUtils;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.wetidiots.game.Game;
import com.wetidiots.game.models.Answer;
import com.wetidiots.game.models.Question;
import com.wetidiots.game.sprites.Avatar;
import com.wetidiots.game.sprites.Box;
import com.wetidiots.game.sprites.Water;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

public class PlayView extends View{

    private Skin skin;
    private Stage stage;

    private TextButton alternative1;
    private TextButton alternative2;
    private TextButton alterantive3;
    private TextButton alternative4;
    private TextButton[] ans_boxes = {alternative1 ,alternative2 ,alterantive3 ,alternative4};
    private ImageButton back_btn;
    private TextButton leaderboard;

    private Water water;
    float limit;
    private Avatar player1Avatar;
    private Avatar player2Avatar;
    private Avatar player3Avatar;
    private Avatar player4Avatar;
    private Avatar[] avatars;

    private ArrayList<Box> a_boxes = new ArrayList<>();
    private ArrayList<Box> b_boxes = new ArrayList<>();
    private ArrayList<Box> c_boxes = new ArrayList<>();
    private ArrayList<Box> d_boxes = new ArrayList<>();
    private ArrayList<ArrayList<Box>> boxes = new ArrayList<>();

    private BitmapFont font;
    private BitmapFont fontTime = new BitmapFont();
    private BitmapFont fontFeedback = new BitmapFont();
    private Label died;

    private Texture questionBox = new Texture("firkant.png");
    private Label question;

    private int questionsInGame = 5;
    private Question currentQuestion;
    private boolean hasAnswered;
    private String username;
    private String feedback;
    private long questionStartTime;
    private int playersSize;

    private Label.LabelStyle ansStyle;
    private Label label1;
    private Label label2;
    private Label label3;
    private Label label4;
    private Label[] ans_labels = {label1,label2, label3, label4};
    private int selfPositionPlayers;

    public PlayView(GameStateManager gsm) {
        super(gsm);
        Gdx.gl.glClearColor(1,1, 1,1);
        StretchViewport viewport = new StretchViewport(Game.WIDTH,Game.HEIGHT,cam);
        stage = new Stage(viewport);
        Gdx.input.setInputProcessor(stage);
        skin = new Skin(Gdx.files.internal("skin/flat-earth-ui.json"));
        font = new BitmapFont();

        questionStartTime = TimeUnit.MILLISECONDS.toSeconds(TimeUtils.millis());
        font.setColor(Color.BLACK);
        font.getData().setScale(1.2f, 1.4f);
        fontTime.setColor(Color.TEAL);
        fontTime.getData().setScale(2.6f, 3.2f);
        fontFeedback.setColor(Color.RED);
        fontFeedback.getData().setScale(3f,3.8f);

        ansStyle = new Label.LabelStyle(font, Color.BLACK);
        died = new Label("", skin);
        died.setWidth(200);
        died.setHeight(200);
        died.setPosition(100,200);
        stage.addActor(died);

        question = new Label("", skin);
        question.setWidth(Game.WIDTH-10);
        question.setHeight(200);
        question.setWrap(true);
        question.setAlignment(Align.left);
        question.setPosition(10,Game.HEIGHT-220);
        stage.addActor(question);

        player1Avatar = new Avatar("im", 50);
        a_boxes.add(new Box(50, 40));
        playersSize = Game.gameController.getGame().getPlayers().size();
        Avatar[] avatars1 = {player1Avatar};
        avatars = avatars1;
        if(playersSize == 2){
            b_boxes.add(new Box( 150,40));
            player2Avatar = new Avatar("bw", 150);
            Avatar[] avatars2 = {player1Avatar, player2Avatar};
            avatars = avatars2;
        }
        else if(playersSize == 3){
            b_boxes.add(new Box(150,40));
            player2Avatar = new Avatar("bw", 150);
            c_boxes.add(new Box(250,40));
            player3Avatar = new Avatar("ca", 250);
            Avatar[] avatars3 = {player1Avatar, player2Avatar, player3Avatar};
            avatars = avatars3;
        }
        else if(playersSize == 4){
            b_boxes.add(new Box(150,40));
            player2Avatar = new Avatar("bw", 150);
            c_boxes.add(new Box(250,40));
            player3Avatar = new Avatar("ca", 250);
            d_boxes.add(new Box(350,40));
            player4Avatar = new Avatar("cm", 350);
            Avatar[] avatars4 = {player1Avatar, player2Avatar, player3Avatar, player4Avatar};
            avatars = avatars4;
        }
        water = new Water(cam.position.x-cam.viewportWidth/2, -Game.HEIGHT+30);
        limit =  water.getPos().y +10;

        back_btn = new ImageButton(skin);
        back_btn.getStyle().imageUp = new TextureRegionDrawable(new TextureRegion(new Texture("back.png")));
        back_btn.setWidth(60);
        back_btn.setHeight(50);
        back_btn.setPosition(0,Game.HEIGHT-back_btn.getHeight()+5);

        leaderboard = new TextButton("Go to leaderboard", skin);
        leaderboard.setWidth(250);
        leaderboard.setHeight(70);
        leaderboard.setPosition(-leaderboard.getWidth(), 350);

        username = Game.userController.getUser().getUserName();
        hasAnswered = false;
        currentQuestion = Game.gameController.getNextQuestion();
        feedback = "";
        addBackLeaderBoardListener();
        if(username.equals(Game.gameController.getUsernamePlayer(0))){
            back_btn.setPosition(-back_btn.getWidth(), 400);
            back_btn.setTouchable(Touchable.disabled);
        }
        stage.addActor(leaderboard);
        stage.addActor(back_btn);
    }

    // Method for placing answer boxes in correct place
    private float[] getAnswerDets(int i) {
        float[] dets = {0,0,200,120};
        float x1 = Game.WIDTH / 16;
        float x2 = Game.WIDTH-(Game.WIDTH/16+200);
        float[] xs = {x1, x2, x1, x2};
        float y1 = 650;
        float y2 = 500;
        float[] ys = {y1,y1,y2,y2};
        dets[0] = xs[i];
        dets[1] = ys[i];
        return dets;
    }

    // Check if your avatar is drowned
    private boolean isDrowned(Avatar avatar){
        if ((avatar.getY()+45) < (water.getPos().y-10+Game.HEIGHT)){
            avatar.die();
            return true;
        }
        return false;
    }

    // Methods for displaying all the players correctly
    private void displayPlayer1(){
        int player1Boxes = Game.gameController.getPlayerBoxes(0);
        if(!isDrowned(player1Avatar) && a_boxes.size() != player1Boxes){
            while(a_boxes.size() < player1Boxes){
                a_boxes.add(new Box(50, (int) (a_boxes.get(a_boxes.size()-1).getY()+10)));
                if (!player1Avatar.isDead()){
                    player1Avatar.updatePos();
                }
            }
        }
    }

    private void displayPlayer2(){
        int player2Boxes = Game.gameController.getPlayerBoxes(1);
        if(!isDrowned(player2Avatar) && b_boxes.size() != player2Boxes){
            while(b_boxes.size() < player2Boxes){
                b_boxes.add(new Box(150, (int) (b_boxes.get(b_boxes.size()-1).getY()+10)));
                if (!player2Avatar.isDead()){
                    player2Avatar.updatePos();
                }
            }
        }
    }

    private void displayPlayer3(){
        int player3Boxes = Game.gameController.getPlayerBoxes(2);
        if(!isDrowned(player3Avatar) && c_boxes.size() != player3Boxes){
            while(c_boxes.size() < player3Boxes){
                c_boxes.add(new Box(250, (int) (c_boxes.get(c_boxes.size()-1).getY()+10)));
                if (!player3Avatar.isDead()){
                    player3Avatar.updatePos();
                }
            }
        }
    }

    private void displayPlayer4(){
        int player4Boxes = Game.gameController.getPlayerBoxes(3);
        if(!isDrowned(player4Avatar) && d_boxes.size() != player4Boxes){
            while(d_boxes.size() < player4Boxes){
                d_boxes.add(new Box(350, (int) (d_boxes.get(d_boxes.size()-1).getY()+10)));
                if (!player4Avatar.isDead()){
                    player4Avatar.updatePos();
                }
            }
        }
    }

    // Update died label if you die
    private void updateDied(){
        selfPositionPlayers = Game.gameController.getGame().findCorrectHashMap(username);
        if(selfPositionPlayers == 0){
            if (player1Avatar.isDead()){
                died.setText("You died!");
                died.setPosition(50, player1Avatar.getY()+100);
            }
        }
        else if(selfPositionPlayers == 1){
            if (player2Avatar.isDead()){
                died.setText("You died!");
                died.setPosition(150, player2Avatar.getY()+100);
            }
        }
        else if(selfPositionPlayers == 2){
            if (player3Avatar.isDead()){
                died.setText("You died!");
                died.setPosition(250, player2Avatar.getY()+100);
            }
        }
        else if(selfPositionPlayers == 3){
            if (player4Avatar.isDead()){
                died.setText("You died!");
                died.setPosition(350, player2Avatar.getY()+100);
            }
        }
    }

    // Calling when the state of the game is changed
    public void stateChanged(){
        displayPlayer1();
        if(Game.gameController.getGame().getPlayers().size() == 2){
            displayPlayer2();
        }
        else if(Game.gameController.getGame().getPlayers().size() == 3){
            displayPlayer2();
            displayPlayer3();
        }
        else if (Game.gameController.getGame().getPlayers().size() == 4){
            displayPlayer2();
            displayPlayer3();
            displayPlayer4();
        }
        updateDied();
    }

    private void registerRightAnswer() {
        feedback = "Right answer!";
        Game.gameController.correctAnswer(username, calculatePoints());
        stateChanged();
        hasAnswered = true;
    }

    private void registerWrongAnswer() {
        feedback = "Wrong answer!";
        Game.gameController.incorrectAnswer(username);
        hasAnswered = true;
        stateChanged();
    }
    // Add listeneres to the answer boxes
    private void addListener(final TextButton box, final Answer[] answers) {
        if (box.getListeners().size > 0){
            box.removeListener(box.getListeners().get(0));
        }
        box.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y){
                if((!hasAnswered) && (!isDrowned(avatars[selfPositionPlayers]))) { // check if player is still alive
                    for (int i = 0; i < answers.length; i++) {
                        if (answers[i] != null) {
                            if (answers[i].isRightAnswer() && box.getText().toString().equals(answers[i].toString())) { // player answered correct
                                registerRightAnswer();
                            } else {
                                if (!answers[i].isRightAnswer() && box.getText().toString().equals(answers[i].toString())) { // player answered wrong
                                    registerWrongAnswer();
                                }
                            }
                        }
                    }
                }
            }
        });
    }

    // Creates answerboxes and adds to stage
    private void createQuestions(final Answer[] answers){
        if (!stage.getActors().isEmpty() && stage.getActors().size>4) { // Clear answers from previous question
            stage.getActors().removeRange(4,stage.getActors().size-1);
        }
        for (int i=0; i < answers.length; i++) {
            if(answers[i] !=null) {
                ans_boxes[i] = new TextButton("", skin);
                ans_boxes[i].setPosition(getAnswerDets(i)[0], getAnswerDets(i)[1]);
                ans_boxes[i].setWidth(getAnswerDets(i)[2]);
                ans_boxes[i].setHeight(getAnswerDets(i)[3]);
                ans_labels[i] = new Label(answers[i].getAnswerText(),skin);
                ans_labels[i].setWrap(true);
                ans_labels[i].setAlignment(Align.center);
                ans_boxes[i].setLabel(ans_labels[i]);
                addListener(ans_boxes[i], answers);
                stage.addActor(ans_boxes[i]);
            }
        }
    }

    private void addBackLeaderBoardListener(){
        back_btn.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y){
                if(username.equals(Game.gameController.getUsernamePlayer(0))){ // back button is not visible for creator
                    Game.gameController.deleteGame();
                }
                else{ // delete yourself from database if you go back during a game
                    Game.gameController.deletePlayer(Game.gameController.getGame().findCorrectHashMap(username), Game.gameController.getGame().getGamePin());
                    Game.gameController.setReady(false);
                }
                Game.gameController.clearGameController();
                try {
                    TimeUnit.MILLISECONDS.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                gsm.set(new MainMenuView(gsm));
            }
        });
        leaderboard.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y){
                gsm.set(new LocalLeaderboardView(gsm));
            }
        });
    }

    private int calculatePoints() {
        int timePassed = (int)(TimeUtils.millis() - TimeUnit.SECONDS.toMillis(questionStartTime));
        return 1000 - timePassed/10;
    }

    // Checking if all players of current game is dead
    private boolean allPlayersDead(){
        if(Game.gameController.getGame().getPlayers().size() == 2){
            if(player1Avatar.isDead() && player2Avatar.isDead()){
                return true;
            }
        }
        else if(Game.gameController.getGame().getPlayers().size() == 3){
            if(player1Avatar.isDead() && player2Avatar.isDead() && player3Avatar.isDead()){
                return true;
            }
        }
        else if(Game.gameController.getGame().getPlayers().size() == 4){
            if(player1Avatar.isDead() && player2Avatar.isDead() && player3Avatar.isDead() && player4Avatar.isDead()){
                return true;
            }
        }
        return false;
    }

    @Override
    public void handleInput() {
    }

    @Override
    public void update(float dt) {
        handleInput();
        if (allPlayersDead()) { // game is finished and button for changing to LocalLeadrboardView is visible
            leaderboard.setPosition(113, 450);
        }
        else {
            Answer[] answers = currentQuestion.getAnswers(); // Get answer alternatives of current question
            createQuestions(answers);
            if (water.getPos().y < limit) {
                water.updatePos(20 * dt);
            }
            if ((TimeUnit.MILLISECONDS.toSeconds(TimeUtils.millis()) - questionStartTime) >= 10L) { // 10 seconds to answer each questioin
                if (username.equals(Game.gameController.getUsernamePlayer(0))) {
                    Game.gameController.moveToNextQ();
                    Game.gameController.updateQuestions(Game.gameController.getGame().getCurrentQ(), Game.gameController.getGame().getNextQ());
                }
                else {
                    try {
                        TimeUnit.SECONDS.sleep(2);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                if (!hasAnswered) {
                    registerWrongAnswer();
                }
                water.timeHasPassed();
                limit = water.getPos().y + 10;
                feedback = "";
                questionStartTime = TimeUnit.MILLISECONDS.toSeconds(TimeUtils.millis());
                hasAnswered = false;
                if (username.equals(Game.gameController.getUsernamePlayer(0))) { //
                    currentQuestion = Game.gameController.getNextQuestion();
                }
               else{
                    currentQuestion = Game.gameController.getNextNewQuestion();
                }
            }
            stateChanged();
            cam.update();
        }
    }

    @Override
    public void render(SpriteBatch sb) {
        sb.setProjectionMatrix(cam.combined);
        sb.begin();
        sb.draw(questionBox, -20, Game.HEIGHT-200, Game.WIDTH+40, 110*adjust);
        question.setText(currentQuestion.getQuestionText());
        //draw inn avatars if ref not null
        for (int i=0; i < playersSize; i++) {
            if (avatars[i] != null) {
                avatars[i].draw(sb);
                font.draw(sb, Game.gameController.getUsernamePlayer(i), avatars[i].getX(), 40f);
                font.draw(sb, String.valueOf(Game.gameController.getPlayersScore(i)), avatars[i].getX(),20f);
            }
        }
        stage.draw();
        //draw boxes for player1
        for (int i = 0; i < a_boxes.size(); i++){
            a_boxes.get(i).draw(sb);
        }
        //boxes for player2
        for (int i = 0; i < b_boxes.size(); i++){
            b_boxes.get(i).draw(sb);
        }
        //boxes for player3
        for (int i = 0; i < c_boxes.size(); i++){
            c_boxes.get(i).draw(sb);
        }
        //boxes for player4
        for (int i = 0; i < d_boxes.size(); i++){
            d_boxes.get(i).draw(sb);
        }
        sb.draw(water.getTexture(), water.getPos().x, water.getPos().y, Game.WIDTH, Game.HEIGHT);
        if (!allPlayersDead()){ // only draw feedback and time left when some players are still alive
            fontTime.draw(sb, Long.toString(10L-(TimeUnit.MILLISECONDS.toSeconds(TimeUtils.millis())-questionStartTime)), Game.WIDTH/2-10, 480);
            fontFeedback.draw(sb, feedback,Game.WIDTH/2-110, 600);
        }
        sb.end();
    }

    @Override
    public void dispose() {
        font.dispose();
        questionBox.dispose();
        water.dispose();
        fontTime.dispose();
        fontFeedback.dispose();
        for (int i = 0; i < a_boxes.size(); i++){
            a_boxes.get(i).dispose();
        }
        for (int i = 0; i < b_boxes.size(); i++){
            b_boxes.get(i).dispose();
        }
        for (int i = 0; i < c_boxes.size(); i++){
            c_boxes.get(i).dispose();
        }
        for (int i = 0; i < d_boxes.size(); i++){
            d_boxes.get(i).dispose();
        }
    }
}