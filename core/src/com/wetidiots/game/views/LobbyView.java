package com.wetidiots.game.views;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.wetidiots.game.Game;

import java.util.concurrent.TimeUnit;

public class LobbyView extends View {

    private Stage stage;
    private Skin skin;

    private ImageButton back_btn;
    private Texture water;
    private Label wait;
    private Label gamePin;
    private Label players;
    private TextButton start;

    private String text1 = "Waiting for players...";
    private String text2 = "Game pin: ";

    public LobbyView(GameStateManager gsm){
        super(gsm);
        Gdx.gl.glClearColor(1,1, 1,1);
        StretchViewport viewport = new StretchViewport(Game.WIDTH,Game.HEIGHT,cam);
        stage = new Stage(viewport);
        Gdx.input.setInputProcessor(stage);
        skin = new Skin(Gdx.files.internal("skin/flat-earth-ui.json"));
        water = new Texture("water1.png");
        back_btn = new ImageButton(skin);
        back_btn.getStyle().imageUp = new TextureRegionDrawable(new TextureRegion(new Texture("back.png")));
        back_btn.setWidth(60);
        back_btn.setHeight(60);
        back_btn.setPosition(0,Game.HEIGHT-back_btn.getHeight());
        wait = new Label(text1, skin);
        wait.setPosition(Game.WIDTH/2-120, Game.HEIGHT/2+250);
        text2 += Game.gameController.getGame().getGamePin();
        gamePin = new Label(text2, skin);
        gamePin.setPosition(Game.WIDTH/2-150, Game.HEIGHT/2+350);
        gamePin.setFontScale(1.5f);
        start = new TextButton("Start", skin);
        start.setWidth(150);
        start.setHeight(50);
        start.setPosition(-start.getWidth(),Game.HEIGHT/2-60);
        start.setTouchable(Touchable.disabled);
        players = new Label(Game.gameController.getPlayersIDs(), skin);
        players.setPosition(Game.WIDTH/2-120, Game.HEIGHT/2+100);
        addListeners();
        stage.addActor(back_btn);
        stage.addActor(wait);
        stage.addActor(gamePin);
        stage.addActor(start);
        stage.addActor(players);
        Game.userController.getUserId();
    }

    private void addListeners(){
        back_btn.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y){ // If creator goes back everything related to the created game should be deleted
                Game.gameController.deleteGame();
                Game.gameController.clearGameController();
                gsm.set(new MainMenuView(gsm));
            }
        });
        start.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y){
                Game.gameController.setReady(true); // Set ready to true so the joiners can read updated value from database and their game also starts
                try {
                    TimeUnit.SECONDS.sleep(2);
                    Game.gameController.readToGamePlay();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                gsm.set(new PlayView(gsm));
            }
        });
    }

    @Override
    public void handleInput() {
    }

    @Override
    public void update(float dt) {
        cam.update();
        players.setText(Game.gameController.getPlayersIDs()); // Show the players currently in the game
        if (Game.gameController.getPlayers().size() <= 4 && Game.gameController.getPlayers().size() >= 2){
            start.setTouchable(Touchable.enabled); // enable and move start button when enough players have joined
            start.setPosition(Game.WIDTH/2-start.getWidth()/2,Game.HEIGHT/2-60);
        }
    }

    @Override
    public void render(SpriteBatch sb) {
        sb.setProjectionMatrix(cam.combined);
        sb.begin();
        stage.draw();
        sb.draw(water, 0, 0, Game.WIDTH, Game.HEIGHT/2-70);
        sb.end();
    }

    @Override
    public void dispose() {
        water.dispose();
        stage.dispose();
    }
}
