package com.wetidiots.game.views;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.wetidiots.game.Game;
import com.wetidiots.game.models.Leaderboard;


public class LeaderboardView extends View{

    private BitmapFont font;
    private Label header;
    private Texture water;
    private ImageButton back_btn;
    private Stage stage;
    private Skin skin;
    private Leaderboard leaderboard;


    protected LeaderboardView(GameStateManager gsm) {
        super(gsm);
        Gdx.gl.glClearColor(1,1, 1,1);
        StretchViewport viewport = new StretchViewport(Game.WIDTH,Game.HEIGHT,cam);
        stage = new Stage(viewport);
        Gdx.input.setInputProcessor(stage);
        skin = new Skin(Gdx.files.internal("skin/flat-earth-ui.json"));

        back_btn = new ImageButton(skin);
        back_btn.getStyle().imageUp = new TextureRegionDrawable(new TextureRegion(new Texture("back.png")));
        back_btn.setWidth(60);
        back_btn.setHeight(60);
        back_btn.setPosition(0,Game.HEIGHT-back_btn.getHeight());
        addListeners();
        stage.addActor(back_btn);

        water = new Texture("water1.png");
        font = new BitmapFont(Gdx.files.internal("Unnamed.fnt"));
        font.getData().setScale(0.30f,0.30f);
        header = new Label("Global Highscores", skin, "title");
        header.setWidth(Game.WIDTH);
        header.setHeight(50);
        header.setAlignment(Align.center);
        header.setPosition(0, 800);
        leaderboard = new Leaderboard();
        stage.addActor(header);

    }

    private void addListeners(){
        back_btn.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y){
                gsm.set(new MainMenuView(gsm));
            }
        });
    }

    @Override
    public void handleInput() {
    }

    @Override
    public void update(float dt) {
        cam.update();
    }

    @Override
    public void render(SpriteBatch sb) {
        sb.setProjectionMatrix(cam.combined);
        sb.begin();
        stage.draw();
        sb.draw(water, 0, 0, 360*adjust, 236*adjust);

        // Draw the globalLeaderboard to screen
        font.draw(sb, "1.", 5*adjust, 520*adjust);
        font.draw(sb, "2.", 5*adjust, 500*adjust);
        font.draw(sb, "3.", 5*adjust, 480*adjust);
        font.draw(sb, "4.", 5*adjust, 460*adjust);
        font.draw(sb, "5.", 5*adjust, 440*adjust);
        font.draw(sb, "6.", 5*adjust, 420*adjust);
        font.draw(sb, "7.", 5*adjust, 400*adjust);
        font.draw(sb, "8.", 5*adjust, 380*adjust);
        font.draw(sb, "9.", 5*adjust, 360*adjust);
        font.draw(sb, "10.", 5*adjust, 340*adjust);
        if (leaderboard.getScoreList().size() != 0){
            font.draw(sb, leaderboard.getScoreList().get(0).toString(), 50*adjust, 520*adjust);
            font.draw(sb, leaderboard.getScoreList().get(2).toString(), 160*adjust,520*adjust);
            font.draw(sb, leaderboard.getScoreList().get(3).toString(), 50*adjust, 500*adjust);
            font.draw(sb, leaderboard.getScoreList().get(5).toString(), 160*adjust, 500*adjust);
            font.draw(sb, leaderboard.getScoreList().get(6).toString(), 50*adjust, 480*adjust);
            font.draw(sb, leaderboard.getScoreList().get(8).toString(), 160*adjust, 480*adjust);
            font.draw(sb, leaderboard.getScoreList().get(9).toString(), 50*adjust, 460*adjust);
            font.draw(sb, leaderboard.getScoreList().get(11).toString(), 160*adjust, 460*adjust);
            font.draw(sb, leaderboard.getScoreList().get(12).toString(), 50*adjust, 440*adjust);
            font.draw(sb, leaderboard.getScoreList().get(14).toString(), 160*adjust, 440*adjust);
            font.draw(sb, leaderboard.getScoreList().get(15).toString(), 50*adjust, 420*adjust);
            font.draw(sb, leaderboard.getScoreList().get(17).toString(), 160*adjust, 420*adjust);
            font.draw(sb, leaderboard.getScoreList().get(18).toString(), 50*adjust, 400*adjust);
            font.draw(sb, leaderboard.getScoreList().get(20).toString(), 160*adjust, 400*adjust);
            font.draw(sb, leaderboard.getScoreList().get(21).toString(), 50*adjust, 380*adjust);
            font.draw(sb, leaderboard.getScoreList().get(23).toString(), 160*adjust, 380*adjust);
            font.draw(sb, leaderboard.getScoreList().get(24).toString(), 50*adjust, 360*adjust);
            font.draw(sb, leaderboard.getScoreList().get(26).toString(), 160*adjust, 360*adjust);
            font.draw(sb, leaderboard.getScoreList().get(27).toString(), 50*adjust, 340*adjust);
            font.draw(sb, leaderboard.getScoreList().get(29).toString(), 160*adjust, 340*adjust);
        }
        sb.end();
    }

    @Override
    public void dispose() {
        stage.dispose();
        font.dispose();
    }
}
