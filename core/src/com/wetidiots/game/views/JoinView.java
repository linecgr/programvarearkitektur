package com.wetidiots.game.views;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.wetidiots.game.Game;
import com.wetidiots.game.controllers.GameController;

import java.util.HashMap;
import java.util.concurrent.TimeUnit;

public class JoinView extends View{

    private Stage stage;
    private Skin skin;
    private TextField gamePin;
    private Label pinLabel;
    public TextButton join_btn;
    private Label error;
    private Label wait;
    private ImageButton back_btn;
    private HashMap player;
    public String errorMessage = "";
    public String joined = "";
    private boolean alreadyActive = false;

    public JoinView(GameStateManager gsm) {
        super(gsm);
        Gdx.gl.glClearColor(1,1, 1,1);
        StretchViewport viewport = new StretchViewport(Game.WIDTH,Game.HEIGHT,cam);
        stage = new Stage(viewport);
        Gdx.input.setInputProcessor(stage);
        skin = new Skin(Gdx.files.internal("skin/flat-earth-ui.json"));

        gamePin = new TextField("", skin);
        gamePin.setWidth(200);
        gamePin.setHeight(50);
        gamePin.setPosition(Game.WIDTH/2-gamePin.getWidth()/2, Game.HEIGHT/2);
        pinLabel = new Label("Enter Game Pin: ", skin);
        pinLabel.setPosition(Game.WIDTH/2-80,Game.HEIGHT/2+60);
        join_btn = new TextButton("Join", skin);
        join_btn.setWidth(150);
        join_btn.setHeight(50);
        join_btn.setPosition(Game.WIDTH/2-join_btn.getWidth()/2,Game.HEIGHT/2-60);

        error = new Label(errorMessage, skin);
        error.setPosition(Game.WIDTH/2-100,Game.HEIGHT/2-75);
        wait = new Label(joined, skin);
        wait.setPosition(Game.WIDTH/2-200, Game.HEIGHT/2+150);
        wait.setFontScale(1.5f);
        back_btn = new ImageButton(skin);
        back_btn.getStyle().imageUp = new TextureRegionDrawable(new TextureRegion(new Texture("back.png")));
        back_btn.setWidth(60);
        back_btn.setHeight(60);
        back_btn.setPosition(0,Game.HEIGHT-back_btn.getHeight());
        addListeners();

        stage.addActor(gamePin);
        stage.addActor(pinLabel);
        stage.addActor(join_btn);
        stage.addActor(error);
        stage.addActor(wait);
        stage.addActor(back_btn);
        player = Game.gameController.getGame().getPlayer1();
    }

    private boolean isValidGamePin(String gamePin){
        if (gamePin.length()<6){
            errorMessage = "Not a valid gamePin";
            return false;
        }
        return true;
    }

    private void addListeners(){
        join_btn.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y){
                if (isValidGamePin(gamePin.getText())){ //check if gamePin is long enough
                    try {
                        Game.gameController.isGameActive(gamePin.getText());
                        TimeUnit.MILLISECONDS.sleep(500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    if(!Game.gameController.getGame().isReady()){ // check if game is ready
                        Game.gameController.getGame().updateGamePin(gamePin.getText());
                        Game.gameController.joinGame(gamePin.getText(), player, JoinView.this);
                        try {
                            TimeUnit.MILLISECONDS.sleep(500);
                            if (errorMessage.length()<1) { // If there is no errormessage joining was successfill
                                Game.gameController.readToGamePlay(); // Read values from Realtime Database of the joined game
                                join_btn.setTouchable(Touchable.disabled); // Disable join-button
                                join_btn.setPosition(-join_btn.getWidth(), Game.HEIGHT/2-60); //Remove join-button from screen
                                gamePin.setDisabled(true); // Disable gamePin textField
                            }
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                    else{
                        alreadyActive = true;
                        errorMessage = "Game has already started";
                    }
                }
            }
        });
        back_btn.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y){
                Game.gameController.clearGameController();
                gsm.set(new MainMenuView(gsm));
            }
        });
    }

    @Override
    public void handleInput() {
    }

    @Override
    public void update(float dt) {
        cam.update();
        error.setText(errorMessage);
        if (!alreadyActive){ // If game is not alreadyactive show the waiting label
            wait.setText(joined);
        }
        if (Game.gameController.getGame().isReady()){ // If game is ready change to PlayView -> start the game
            try {
                TimeUnit.MILLISECONDS.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            gsm.push(new PlayView(gsm));
        }
    }

    @Override
    public void render(SpriteBatch sb) {
        sb.setProjectionMatrix(cam.combined);
        sb.begin();
        stage.draw();
        sb.end();
    }

    @Override
    public void dispose() {
        stage.dispose();
    }
}
