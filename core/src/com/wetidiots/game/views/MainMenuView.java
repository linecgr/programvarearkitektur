package com.wetidiots.game.views;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.wetidiots.game.Game;
import com.wetidiots.game.sprites.Water;

public class MainMenuView extends View{

    private Skin skin;
    private Stage stage;

    private Texture logo;
    private Water water;

    private TextButton create_btn;
    private TextButton join_btn;
    private TextButton tutorial;
    private TextButton lb;
    private ImageButton settings_btn;

    private Message message;

    public MainMenuView(GameStateManager gsm){
        super(gsm);
        Gdx.gl.glClearColor(1,1, 1,1);
        StretchViewport viewport = new StretchViewport(Game.WIDTH,Game.HEIGHT,cam);
        stage = new Stage(viewport);
        Gdx.input.setInputProcessor(stage);
        skin = new Skin(Gdx.files.internal("skin/flat-earth-ui.json"));
        message = new Message();

        logo = new Texture("logo.png");
        water = new Water(cam.position.x-cam.viewportWidth/2, -Game.HEIGHT);
        create_btn = new TextButton("Create game", skin);
        create_btn.setWidth(147);
        create_btn.setHeight(70);
        create_btn.setPosition(Game.WIDTH/2-create_btn.getWidth()-5,Game.HEIGHT/2+50);
        join_btn = new TextButton("Join game", skin);
        join_btn.setPosition(Game.WIDTH/2+5,Game.HEIGHT/2+50);
        join_btn.setWidth(147);
        join_btn.setHeight(70);
        tutorial = new TextButton("Tutorial", skin);
        tutorial.setPosition(Game.WIDTH/2-create_btn.getWidth()-5,Game.HEIGHT/2-create_btn.getHeight()+40);
        tutorial.setWidth(147);
        tutorial.setHeight(70);
        lb = new TextButton("Leaderboard", skin);
        lb.setPosition(Game.WIDTH/2+5,Game.HEIGHT/2-create_btn.getHeight()+40);
        lb.setWidth(147);
        lb.setHeight(70);
        settings_btn = new ImageButton(skin);
        settings_btn.getStyle().imageUp = new TextureRegionDrawable(new TextureRegion(new Texture("settings.png")));
        settings_btn.setWidth(60);
        settings_btn.setHeight(60);
        settings_btn.setPosition(Game.WIDTH-settings_btn.getWidth(),Game.HEIGHT-settings_btn.getHeight());

        addListeners();
        stage.addActor(create_btn);
        stage.addActor(join_btn);
        stage.addActor(tutorial);
        stage.addActor(lb);
        stage.addActor(settings_btn);
    }

    private void addListeners(){
        // Initate yourself as a player and move to corresponding view depending on button pressed
        create_btn.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y){ // create a game if this button is pressed
                Game.gameController.startGame();
                Game.gameController.getGame().initiateSelf(Game.userController.getUser().getUserName(), Game.userController.getUserId());
                Game.gameController.getGame().setGamePin(6);
                Game.gameController.createGame();
                message.generateInfoMessage("Loading game...").show(stage).setPosition(90,390);
                gsm.set(new LobbyView(gsm));
            }
        });
        join_btn.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y){
                Game.gameController.getGame().initiateSelf(Game.userController.getUser().getUserName(), Game.userController.getUserId());
                gsm.set(new JoinView(gsm));
            }
        });
        tutorial.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y){
                Game.gameController.getGame().initiateSelf(Game.userController.getUser().getUserName(), Game.userController.getUserId());
                gsm.set(new TutorialView(gsm));
            }
        });
        lb.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y){
                Game.gameController.getGame().initiateSelf(Game.userController.getUser().getUserName(), Game.userController.getUserId());
                gsm.set(new LeaderboardView(gsm));
            }
        });
        settings_btn.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y){
                Game.gameController.getGame().initiateSelf(Game.userController.getUser().getUserName(), Game.userController.getUserId());
                gsm.set(new SettingsView(gsm));
            }
        });
    }

    @Override
    public void handleInput() {
    }

    @Override
    public void update(float dt) {
        handleInput();
        water.updatePos(50*dt);
        cam.update();
    }

    @Override
    public void render(SpriteBatch sb) {
        sb.setProjectionMatrix(cam.combined);
        sb.begin();
        stage.draw();
        sb.draw(logo, Game.WIDTH/2-200, 700, 400,160);
        sb.draw(water.getTexture(), water.getPos().x, water.getPos().y, Game.WIDTH, Game.HEIGHT);
        sb.end();
    }

    @Override
    public void dispose() {
        logo.dispose();
        water.dispose();
        stage.dispose();
    }
}
