package com.wetidiots.game.views;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.wetidiots.game.Game;
import com.wetidiots.game.controllers.UserController;

import java.util.concurrent.TimeUnit;


public class LogInView extends View{
    private Stage stage;
    private Skin skin;
    private OrthographicCamera camera;
    private UserController userController;
    private Message message;
    private Texture header;

    private Label emailLabel;
    private Label passwordLabel;

    private TextField emailTf;
    private TextField passwordTf;

    private ImageButton back_btn;
    private TextButton submitBtn;


    public LogInView(final GameStateManager gsm) {
        super(gsm);
        this.userController = Game.userController;
        camera = new OrthographicCamera();
        camera.setToOrtho(false, Game.WIDTH, Game.HEIGHT);
        StretchViewport viewp = new StretchViewport(Game.WIDTH, Game.HEIGHT, camera);
        Gdx.gl.glClearColor(1,1, 1,1);

        stage = new Stage(viewp);
        Gdx.input.setInputProcessor(stage);
        skin = new Skin(Gdx.files.internal("skin/flat-earth-ui.json"));
        message = new Message();

        header = new Texture("LoginHeader.png");

        emailLabel = new Label("Email",skin);
        emailLabel.setPosition(90,570);

        emailTf = new TextField("", skin);
        emailTf.setSize(300,60);
        emailTf.setPosition(Game.WIDTH/2- emailTf.getWidth()/2,510);

        passwordLabel = new Label("Password",skin);
        passwordLabel.setPosition(90,425);

        passwordTf = new TextField("", skin);
        passwordTf.setSize(300,60);
        passwordTf.setPosition(Game.WIDTH/2-passwordTf.getWidth()/2,365);

        submitBtn = new TextButton("Login", skin);
        submitBtn.setSize(140,60);
        submitBtn.setPosition(Game.WIDTH/2- submitBtn.getWidth()/2,250);
        submitBtn.setColor(0.52f, 0.81f, 0.41f, 1);

        back_btn = new ImageButton(skin);
        back_btn.getStyle().imageUp = new TextureRegionDrawable(new TextureRegion(new Texture("back.png")));
        back_btn.setSize(90,90);
        back_btn.setPosition(0,Game.HEIGHT-back_btn.getHeight());
        addListeners();

        stage.addActor(emailLabel);
        stage.addActor(passwordLabel);
        stage.addActor(emailTf);
        stage.addActor(passwordTf);
        stage.addActor(submitBtn);
        stage.addActor(back_btn);
    }

    private void addListeners(){
        //checks if the user is able to login. If login is successful it sends the user to mainmenu, if not displays errormessage
        submitBtn.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y){
                try {
                    if(!userController.logIn(userController.makeUser(emailTf.getText(),passwordTf.getText()))){
                        Gdx.gl.glClearColor(0.8f,0.8f, 0.8f,1);
                        message.generateErrorMessage(userController.getErrorMessage()).show(stage).setPosition(90,390);
                    }
                    else{
                        TimeUnit.SECONDS.sleep(1);
                        if(!userController.isSignedIn()){
                            Gdx.gl.glClearColor(0.8f,0.8f, 0.8f,1);
                            message.generateErrorMessage(userController.getAuthErrorMessage()).show(stage).setPosition(90,390);
                        }
                        else{
                            Game.userController.setUser();
                            gsm.set(new MainMenuView(gsm));
                        }
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }});
        back_btn.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y){
                gsm.set(new StartScreenView(gsm));
            }
        });
    }

    @Override
    public void handleInput() {

    }

    @Override
    public void update(float dt) {

    }

    @Override
    public void render(SpriteBatch sb) {
        sb.setProjectionMatrix(camera.combined);
        sb.begin();
        sb.draw(header, 68*adjust, 500*adjust, 110*adjust,52*adjust);
        sb.end();
        stage.act();
        stage.draw();
    }

    @Override
    public void dispose() {
        stage.dispose();
        header.dispose();
    }
}
