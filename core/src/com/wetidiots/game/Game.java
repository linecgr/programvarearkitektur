package com.wetidiots.game;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.wetidiots.game.controllers.GameController;
import com.wetidiots.game.controllers.LeaderboardController;
import com.wetidiots.game.views.GameStateManager;
import com.wetidiots.game.views.MainMenuView;

import com.wetidiots.game.controllers.UserController;
import com.wetidiots.game.views.StartScreenView;

public class Game extends ApplicationAdapter {
	FirebaseInterface _FBI;
	public static final String TITLE = "WetIdiots";
	public static final int WIDTH = 480;
	public static final int HEIGHT = 1000;
	private GameStateManager gsm;
	private SpriteBatch sb;
	public static GameController gameController;
	public static UserController userController;
	public static LeaderboardController leaderboardController;

	public static Music music;
	public static com.wetidiots.game.models.Music musicmodel;

	public Game(FirebaseInterface FBI){
		_FBI = FBI;
		gameController = new GameController(_FBI);
		userController = new UserController(_FBI);
		leaderboardController= new LeaderboardController(_FBI);
	}
	
	@Override
	public void create () {
		sb = new SpriteBatch();
		setMusic();
		gsm = GameStateManager.getGsm(); // Singleton pattern
		Gdx.gl.glClearColor(1,1, 1,1);

		setMusic();

		if(userController.isSignedIn()){ // If user already is signed in MainMenuView should appear
			userController.setUser();
			gsm.push(new MainMenuView(gsm));
		}
		else {
			gsm.push(new StartScreenView(gsm)); // Else StartScreenView should apperar
		}
	}

	private void setMusic(){
		musicmodel = new com.wetidiots.game.models.Music();
		music = Gdx.audio.newMusic(Gdx.files.internal("music.mp3"));
		music.setLooping(true);
		music.setVolume(0.05f);
	}

	@Override
	public void render () {
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		gsm.update(Gdx.graphics.getDeltaTime());
		gsm.render(sb);
	}
	
	@Override
	public void dispose () {
		super.dispose();
		music.dispose();
		sb.dispose();
	}
}
