package com.wetidiots.game.models;

public class User {
    private String userName;
    private String email;
    private String password;
    private String errorMessage="";
    private String id;

    public User(String userName, String email, String password) {
        this.userName=userName;
        this.email=email;
        this.password=password;
    }

    public User(){
    }

    public boolean isValidUser(String userName, String email, String password) {
        return isValidUserName(userName) && isValidEmail(email) && isValidPassword(password);
    }

    public boolean isValidUser(String email, String password) {
        return isValidEmail(email) && isValidPassword(password);
    }

    public void setId(String id){
        this.id = id;
    }

    /**
     * Checks if the length of the username is above the minimum (3 characters or longer).
     * Updates the errorMessage variable if not
     * @param username The user's desired username
     * @return true if the username is valid and false if not
     */
    private boolean isValidUserName(String username) {
        if (username.length() < 3) {
            errorMessage="The username must be at least 3 characters long";
            return false;
        }
        return true;
    }

    /**
     * Checks if the email has a proper format
     * Updates the errorMessage variable if not
     * @param email The user's desired email
     * @return true if the email is valid and false if not
     */
    private boolean isValidEmail(String email) {
        String emailFormat = "[A-ZÆØÅa-zæøå_.%&0-9]+@[A-ZÆØÅa-zæøå_.%&0-9]+\\.[a-z]{2,3}";
        if (!email.matches(emailFormat)) {
            errorMessage="The email format is not correct";
            return false;
        }
        return true;
    }


    /**
     * Checks if the password is long enough (8 characters or longer)
     * Updates the errorMessage variable if not
     * @param password The user's desired password
     * @return true if the password is valid and false if not
     */
    private boolean isValidPassword(String password) {
        if (password.length() < 8) {
            errorMessage="The password must be at least 8 characters long";
            return false;
        }
        return true;
    }


    public String getUserName() {
        return userName;
    }
    public void setUserName(String userName) {
        this.userName = userName;
    }
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public String getPassword() {
        return password;
    }
    public String getErrorMessage() {
        return errorMessage;
    }

    @Override
    public String toString() {
        return "User{" +
                "userName='" + userName + '\'' +
                ", email='" + email + '\'' +
                ", id='" + id + '\'' +
                '}';
    }
}
