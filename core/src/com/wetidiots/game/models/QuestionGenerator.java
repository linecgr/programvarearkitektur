package com.wetidiots.game.models;

import com.badlogic.gdx.Gdx;
import com.wetidiots.game.FirebaseInterface;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.TimeUnit;

// Handles the creation of questions and answers
public class QuestionGenerator {

    private int numberOfQuestions;
    private int num_of_quest_for_game;
    private int numberOfUsedQuestions;
    private List<Integer> questionIndexes;
    private List<String> nextNewQuestion;

    private Random rand = new Random();

    public QuestionGenerator(int num_of_quest_for_game) {
        numberOfQuestions = 160;
        numberOfUsedQuestions = 0;
        this.num_of_quest_for_game = num_of_quest_for_game;
        this.questionIndexes = getQuestionIndexes();
        this.nextNewQuestion = new ArrayList<>();
    }

    public void setNextQuestion(List<String> question) {
        this.nextNewQuestion = question;
    }

    // Creates a list with random indexes to use in a game
    private List<Integer> getQuestionIndexes() {
        Set<Integer> usedQuestions = new HashSet<>();
        List questionIndexes = new ArrayList();
        int questNum = 0;
        for (int i = 0; i<= num_of_quest_for_game; i++) {
            // Find a new unused question
            if (usedQuestions.size() < numberOfQuestions) {
                while (true) {
                    // Generates random question number
                    questNum = rand.nextInt(numberOfQuestions);
                    // Check if the number is already used
                    if (!usedQuestions.contains(questNum)){
                        break;
                    }
                }
                usedQuestions.add(questNum);
            }
            else {
                System.out.println("There are not enough questions!");
            }
        }
        questionIndexes = Arrays.asList(usedQuestions.toArray());
        return questionIndexes;
    }

    public List<String> getNextFromDB(FirebaseInterface FBI) {
        List<String> newQuestion = new ArrayList<>();
        if(numberOfUsedQuestions < questionIndexes.size()) {
            if (numberOfUsedQuestions == 0) {
                FBI.readQuestion(String.valueOf(questionIndexes.get(numberOfUsedQuestions)),this);
                while(true) {
                    if (!nextNewQuestion.isEmpty()) {
                        break;
                    }
                }
                numberOfUsedQuestions++;
            }
            newQuestion = nextNewQuestion;
            FBI.readQuestion(String.valueOf(questionIndexes.get(numberOfUsedQuestions)), this);
            try {
                TimeUnit.SECONDS.sleep(2); // To make sure the question is read from Firestore before returning it
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            numberOfUsedQuestions++;
        }
        else {
            newQuestion.clear();
            newQuestion.add("Used up questions for game");
        }
        return newQuestion;
    }

    // Randomizes answers in given answerList
    private ArrayList<Answer> randomizeAnswers(ArrayList<Answer> a_list) {
        ArrayList<Answer> tmpAnswList = new ArrayList<>();
        int currNumAnswers = 0;
        Random answRand = new Random();
        currNumAnswers = a_list.size();
        if (currNumAnswers == 4) {
            tmpAnswList.add(a_list.get(3));
            tmpAnswList.add(answRand.nextInt(2), a_list.get(2));
            tmpAnswList.add(answRand.nextInt(3), a_list.get(1));
            tmpAnswList.add(answRand.nextInt(4), a_list.get(0));
        }
        else
        if (currNumAnswers == 3) {
            tmpAnswList.add(a_list.get(2));
            tmpAnswList.add(answRand.nextInt(2), a_list.get(1));
            tmpAnswList.add(answRand.nextInt(3), a_list.get(0));
        }
        else
        if (currNumAnswers == 2) {
            tmpAnswList.add(a_list.get(1));
            tmpAnswList.add(answRand.nextInt(2), a_list.get(0));
        }
        return tmpAnswList;
    }

    // Creates a Question from a List on the format used in the database
    public Question makeQuestionFromDBList(List<String> questionFromDB) {
        Answer newAnsw;
        ArrayList<Answer> AnswList = new ArrayList<>();
        ArrayList<Answer> randomizedAnswList = new ArrayList<>();
        // Get answers
        int currNumAnswers = questionFromDB.size() - 1;
        newAnsw = new Answer(questionFromDB.get(1), true);
        AnswList.add(newAnsw);
        for (int i = 2; i < currNumAnswers + 1; i++) {
            newAnsw = new Answer(questionFromDB.get(i), false);
            AnswList.add(newAnsw);
        }
        // Put the answers in random order
        randomizedAnswList = randomizeAnswers(AnswList);
        Answer[] answers = randomizedAnswList.toArray(new Answer[0]);

        Question newQuest = new Question(questionFromDB.get(0), answers);
        return newQuest;
    }
}


