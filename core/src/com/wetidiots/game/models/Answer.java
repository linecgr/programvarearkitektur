package com.wetidiots.game.models;

public class Answer {

    private String aText;
    private boolean rightAnswer;

    public Answer(String aText, Boolean rightAnswer) {
        this.aText = aText;
        this.rightAnswer = rightAnswer;
    }

    public CharSequence getAnswerText() {
        return this.aText;
    }

    public boolean isRightAnswer() {
        return rightAnswer;
    }

    @Override
    public String toString() {
        return aText;
    }
}

