package com.wetidiots.game.models;

public class Question {

    private String qText;
    private Answer[] answers;

    public Question(String qText, Answer... answers) {
        this.qText = qText;
        Answer[] a = {null, null, null, null};
        this.answers = a;
        int i;
        for (i = 0; i < answers.length; i++) {
            this.answers[i] = answers[i];
        }
    }

    public String getQuestionText() {
        return qText;
    }

    public Answer[] getAnswers() {
        return answers;
    }
}
