package com.wetidiots.game.models;

import java.security.SecureRandom;

public class Lobby {

    private static String currentGamePin;

    // Randomly generates a gamePin
    public static String generateRandomPassword(int len)
    {
        final String chars = "0123456789abcdefghijklmnopqrstuvwxyz";

        SecureRandom random = new SecureRandom();
        StringBuilder sb = new StringBuilder();
        // Each iteration of the loop randomly chooses a character from the given
        // ASCII range and appends it to the `StringBuilder` instance
        for (int i = 0; i < len; i++)
        {
            int randomIndex = random.nextInt(chars.length());
            sb.append(chars.charAt(randomIndex));
        }
        currentGamePin = sb.toString();
        return currentGamePin;
    }
}
