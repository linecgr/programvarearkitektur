package com.wetidiots.game.models;


import com.wetidiots.game.Game;
import java.util.ArrayList;
import java.util.List;

public class Leaderboard {

    private List<Object> scoreList = new ArrayList<>();

    public Leaderboard() {
        Game.leaderboardController.readScores("users", scoreList);
    }

    // Strips the scoreList read from database
    public List<String> stripScoreList(){
        List<String> strippedScores = new ArrayList<>();
        for (Object o : scoreList) {
            String[] stripped = o.toString().split(",|,|,");
            for (int i=0; i<stripped.length; i++){
                strippedScores.add(stripped[i]);
            }
        }
        return strippedScores;
    }

    public List getScoreList() {
        List<String> myList = stripScoreList();
        return myList;
    }
}