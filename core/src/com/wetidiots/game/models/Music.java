package com.wetidiots.game.models;

public class Music {

    private boolean playingMusic;

    public Music(){
        this.playingMusic = true;
    }

    public boolean getMusic(){
        return this.playingMusic;
    }

    public void setMusic(boolean playingMusic){
        if (playingMusic){
            this.playingMusic = true;
        } else {
            this.playingMusic = false;
        }
    }

    public void musicClicked() {
        if (this.playingMusic){
            setMusic(false);
        } else {
            setMusic(true);
        }
    }
}
