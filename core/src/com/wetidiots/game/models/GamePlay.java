package com.wetidiots.game.models;

import com.wetidiots.game.FirebaseInterface;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static java.util.Map.Entry.*;
import static java.util.stream.Collectors.*;

public class GamePlay {

    private String gamePin;
    private List<HashMap>  players;
    private HashMap<String, Object> player1 = new HashMap<>();
    private HashMap<String, Object> player2 = new HashMap<>();
    private HashMap<String, Object> player3 = new HashMap<>();
    private HashMap<String, Object> player4 = new HashMap<>();
    private List<String> currentQ = new ArrayList<>();
    private List<String> nextQ = new ArrayList<>();
    private boolean ready = false;
    private QuestionGenerator qGenerator;

    public GamePlay(FirebaseInterface FBI){
        gamePin = "";
        players = new ArrayList<>();
        this.qGenerator = new QuestionGenerator(60);
    }

    public void clearGame(){
        this.gamePin = "";
        this.players.clear();
        this.player1.clear();
        this.player2.clear();
        this.player3.clear();
        this.player4.clear();
        this.currentQ.clear();
        this.nextQ.clear();
        this.ready = false;
    }

    public void startGame(FirebaseInterface FBI) {
        currentQ = qGenerator.getNextFromDB(FBI);
        nextQ = qGenerator.getNextFromDB(FBI);
    }

    public void initiateSelf(String username, String id){
        player1.put("username", username);
        player1.put("rights", 0);
        player1.put("wrongs", 0);
        player1.put("id", id);
        player1.put("score", 0);
        player1.put("boxes", 1);
    }

    public List<String> getCurrentQ() {
        return currentQ;
    }
    public List<String> getNextQ() {
        return nextQ;
    }
    public List<HashMap> getPlayers() { return players; }
    public void wipePlayers(){ players.clear(); }
    public String getGamePin() { return gamePin; }
    public HashMap<String, Object> getPlayer1() { return player1; }
    public boolean isReady() { return ready; }
    public void setNextQ(List<String> nextQ) { this.nextQ = nextQ; }
    public void setCurrentQ(List<String> currentQ) { this.currentQ = currentQ; }
    public void setReady(boolean ready) { this.ready = ready; }

    public void updateGamePin(String gamePin){ this.gamePin = gamePin; }

    // Sets a new GamePin with method from Lobby
    public void setGamePin(int len) { this.gamePin = Lobby.generateRandomPassword(len); }

    // creator method to move to next question
    public void moveToNextQ(FirebaseInterface FBI) {
        currentQ = nextQ;
        nextQ = qGenerator.getNextFromDB(FBI);
    }

    // Creates a Question from the currentQ list, used by creator
    public Question newQuestion() {
        return qGenerator.makeQuestionFromDBList(currentQ);
    }

    // Used by joiner
    public Question nextNewQuestion() {
        return qGenerator.makeQuestionFromDBList(nextQ);
    }

    // Find index of correct hashmap to player with given username
    public int findCorrectHashMap(String username){
        if(players.get(0).get("username").equals(username)){
            return 0;
        }
        else if (players.size() >= 2 && !players.get(1).isEmpty() && players.get(1).get("username").equals(username)){
            return 1;
        }
        else if (players.size() >= 3 && !players.get(2).isEmpty() && players.get(2).get("username").equals(username)){
            return 2;
        }
        else if (players.size() == 4 &&!players.get(3).isEmpty() && players.get(3).get("username").equals(username)){
            return 3;
        }
        else{
            return -1;
        }
    }

    // Update targetValue of player with given username
    public void updatePlayer(String username, String targetValue, int pointsRecieved){
        int target = findCorrectHashMap(username);
        if(targetValue == "rights"){
            int currentValue = (Integer) players.get(target).get("rights");
            players.get(target).put(targetValue, currentValue + 1);
            players.get(target).put("boxes", (Integer) players.get(target).get("boxes") + 1);
            players.get(target).put("score", (Integer) players.get(target).get("score") + pointsRecieved);
        }
        else if(targetValue == "wrongs"){
            int currentValue = (Integer) players.get(target).get("wrongs");
            players.get(target).put(targetValue, currentValue + 1);
        }
    }

    // Update a player with value
    private void updatePlayerFromRead(HashMap<String, Object> value, int playerCount) {
        HashMap<String, Object> tempPlayer = new HashMap<>();
        if (players.isEmpty() && playerCount == 0) { // init case
            tempPlayer.put("id", (String) value.get("id"));
            tempPlayer.put("username", (String) value.get("username"));
            tempPlayer.put("boxes", ((Long) value.get("boxes")).intValue());
            tempPlayer.put("rights", ((Long) value.get("rights")).intValue());
            tempPlayer.put("wrongs", ((Long) value.get("wrongs")).intValue());
            tempPlayer.put("score", ((Long) value.get("score")).intValue());
            player1 = tempPlayer;
            players.add(player1);
        } else {
            if (findCorrectHashMap((String) value.get("username")) != -1) { // If the player already exist in local GamePLay
                int target = findCorrectHashMap((String) value.get("username"));
                players.get(target).put("id", (String) value.get("id"));
                players.get(target).put("username", (String) value.get("username"));
                players.get(target).put("boxes", ((Long) value.get("boxes")).intValue());
                players.get(target).put("rights", ((Long) value.get("rights")).intValue());
                players.get(target).put("wrongs", ((Long) value.get("wrongs")).intValue());
                players.get(target).put("score", ((Long) value.get("score")).intValue());
            } else if (players.size() - 1 != playerCount) { // If it 1 or more players
                tempPlayer.put("id", (String) value.get("id"));
                tempPlayer.put("username", (String) value.get("username"));
                tempPlayer.put("boxes", ((Long) value.get("boxes")).intValue());
                tempPlayer.put("rights", ((Long) value.get("rights")).intValue());
                tempPlayer.put("wrongs", ((Long) value.get("wrongs")).intValue());
                tempPlayer.put("score", ((Long) value.get("score")).intValue());
                players.add(tempPlayer);
            }
        }
    }

    private void updateQuestionFromRead(List<String> questionList, List<String> values){
        // Empty questionList
        questionList.clear();
        // Fill the list with values from database
        for(String el : values){
            questionList.add(el);
        }
    }

    // Update this gamePlay with values from database
    public void updateGamePlay(HashMap<String, Object> databaseValues){
        int counter = 0;
        List readPlayers = (List) databaseValues.get("players");
        for(Object player : readPlayers){
            updatePlayerFromRead( (HashMap<String, Object>) player, counter);
            counter++;
        }
        if(!player1.isEmpty()){
            updateQuestionFromRead(currentQ,(List<String>) databaseValues.get("currentQ"));
        }else{
            currentQ = nextQ;
        }
        updateQuestionFromRead(nextQ,(List<String>) databaseValues.get("nextQ"));
        ready = (Boolean) databaseValues.get("ready");
        if(gamePin == null){
            gamePin = (String) databaseValues.get("gamePin");
        }
    }

    // Get a player's number of boxes
    public int playerBoxesByPlayerNumber(int playerNumber){
        return (int) players.get(playerNumber).get("boxes");
    }

    // Get a player's score
    public int playerScoreByPlayerNumber(int playerNumber){
        return (int) players.get(playerNumber).get("score");
    }

    // Creates a sorted list with all players username and score, used to show local leaderboard after a game is finished
    public List getGameScores() {
        Map<Object, Integer> scores = new HashMap();
        for (HashMap player : players){
            scores.put(player.get("username"), (Integer) player.get("score"));
        }
        Map<Object, Integer> sorted = scores.entrySet().stream()
                .sorted(comparingByValue(Comparator.reverseOrder()))
                .collect(
                        toMap(user-> user.getKey(), user->user.getValue(), (u1, u2)->u2, LinkedHashMap::new));
        List<String> sortedList = new ArrayList<>();
        sorted.entrySet().stream().forEach(player -> sortedList.add(""+player.getKey()+": "+player.getValue()));
        return sortedList;
    }
}
