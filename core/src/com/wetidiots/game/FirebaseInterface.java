package com.wetidiots.game;

import com.wetidiots.game.models.GamePlay;
import com.wetidiots.game.models.QuestionGenerator;
import com.wetidiots.game.models.User;
import com.wetidiots.game.views.JoinView;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public interface FirebaseInterface {

    /**
     * Creates a user account in Firebase with the given email and password.
     * Writes the user's email, password and username to Firestore
     * @param email The user's email
     * @param password The user's password
     * @param userName The user's username
     */
    void createUser(String email, String password, String userName);

    /**
     * Checks if the user is signed in
     * @return true if the user is signed in and false if not
     */
    boolean isSignedIn();

    /**
     * Finds the username of the given email
     * @param email The users email
     * @return username of the provided email
     */
    String getUsername(String email);

    /**
     * Sign in the user with the given email and password
     * @param email The user's email
     * @param password The user's password
     */
    void logIn(String email, String password);

    /**
     * Gets userId of current user, sets username and email of given user and adds userId and username to list in User
     * @param userId
     * @param user
     */
    void getUid(List userId, User user);

    /**
     * Updates the user's score in Firestore (adds the new score to the user's total score).
     * @param newScore The user's new score
     */
    void updateGlobalScore(int newScore);

    /**
     * Gets errormessage
     * @return errormessage
     */
    String getErrorMessage();

    /**
     * Signs out the user currently signed in
     */
    void signOut();

    /**
     * Reads a question at given index from firestore and set it as nextQuestion in QuestionGenerator
     * @param index index of a question stored in firestore
     * @param questionGenerator
     */
    void readQuestion(String index, QuestionGenerator questionGenerator);

    /**
     * Sorts collection in Firestore based on the value in score field and adds the 10 highest score to dataLocation
     * @param collection name of collection to order in Firestore
     * @param dataLocation List to store 10 sorted values in
     */
    void orderValuesFromFirestore(String collection, List dataLocation);

    /**
     * Add value to Realtime Database
     * @param target Where to add the value
     * @param value
     */
    void addValuesToRTD(String target, Object value);

    /**
     * Check if gamePin already exist and if not, create a game and add to Realtime Database
     * @param game Gameplay that should be added
     */
    void createGame(GamePlay game);

    /**
     * Deletes given Gameplay from Realtime Database
     * @param game
     */
    void deleteGame(GamePlay game);

    /**
     * Delete player at playerNumber from game given by gamePin
     * @param playerNumber number of player to be deleted
     * @param gamePin of game that player should be deleted from
     */
    void deletePlayer(int playerNumber, String gamePin);

    /**
     * Joins game with gamePin if game exist in Realtime Database and players size is less than 4
     * @param gamePin of game to join
     * @param player Hashmap with values of the player that is joining
     * @param view JoinView to update errorMessage in the View
     */
    void joinGame(String gamePin, HashMap player, JoinView view);

    /**
     * Checks if a game is active
     * @param game GamePlay that should update ready value
     * @param gamePin of game to check
     */
    void isGameActive(GamePlay game, String gamePin);

    /**
     * Sets ready value of a game
     * @param gamePin of game that should be updated
     * @param ready
     */
    void setReady(String gamePin, boolean ready);

    /**
     * Gets value of ready to given game from Realtime Database and updates local Gameplay with value from RD
     * @param game
     */
    void getReady(GamePlay game);

    /**
     * Gets players in given Gameplay and username of players to a list
     * @param game Gameplay to get players from
     * @param ids List to add the usernames to
     */
    void getPlayers(GamePlay game, List ids);

    /**
     * Update player in Realtime Database game with given playerNumber
     * @param game
     * @param playerNumber
     */
    void updatePlayer(GamePlay game, int playerNumber);

    /**
     * Reads values from game in Realtime Database to local Gameplay
     * @param game that should be read
     */
    void readToGamePlay(GamePlay game);

    /**
     * Update currentQ and nextQ in given game in Realtime Database
     * @param game
     * @param currentQ
     * @param nextQ
     */
    void updateQuestions(GamePlay game, List<String> currentQ, List<String> nextQ);

    /**
     * Read currentQ and nextQ from game in Realtime Database and update local Gameplay game
     * @param game
     */
    void readQuestions(GamePlay game);

    /**
     * Update score of player with given playerNumber in Realtime Database with given score
     * @param game
     * @param score
     * @param playerNumber
     */
    void updateScore(GamePlay game, int score, int playerNumber);
}
