package com.wetidiots.game.controllers;

import com.badlogic.gdx.Gdx;
import com.wetidiots.game.FirebaseInterface;
import com.wetidiots.game.models.User;

import java.util.ArrayList;
import java.util.List;

// Connect leaderboard methods with methods to read/write from database/firestore
public class UserController {

    private final FirebaseInterface FBI;
    private User user;
    private List userId = new ArrayList<>();

    public UserController(FirebaseInterface FBI) {
        this.FBI=FBI;
        this.user=new User();
    }

    public User getUser() {
        return user;
    }
    public String getErrorMessage(){return this.user.getErrorMessage();}
    public String getAuthErrorMessage(){return FBI.getErrorMessage();}

    public User makeUser(String userName, String email, String password){
        this.user= new User(userName, email, password);
        return this.user;
    }

    public User makeUser( String email, String password){
        String username=FBI.getUsername(email);
        this.user= new User(username, email, password);
        return this.user;
    }

    public boolean registerUser(User user){
        if(user.isValidUser(user.getUserName(), user.getEmail(), user.getPassword())) {
            FBI.createUser(user.getEmail(), user.getPassword(), user.getUserName());
            return true;
        }
        return false;
    }

    public boolean logIn(User user) {
        if (user.isValidUser(user.getEmail(), user.getPassword())){
            FBI.logIn(user.getEmail(), user.getPassword());
            return true;
        }
        return false;
    }

    public boolean isSignedIn(){return FBI.isSignedIn();}
    public void signOut(){
        FBI.signOut();
    }
    public void setUser(){
        FBI.getUid(this.userId, this.user);
    }

    public String getUserId() {
        user.setId(userId.get(0).toString());
        if (userId.size()>1){
            user.setUserName(userId.get(1).toString());
        }
        return userId.get(0).toString();
    }
}
