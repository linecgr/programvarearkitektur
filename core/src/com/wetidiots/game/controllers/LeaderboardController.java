package com.wetidiots.game.controllers;

import com.wetidiots.game.FirebaseInterface;

import java.util.List;

// Connect leaderboard methods with methods to read/write from database/firestore
public class LeaderboardController {

    private final FirebaseInterface FBI;

    public LeaderboardController(FirebaseInterface FBI) {
        this.FBI = FBI;
    }

    public void readScores(String collection, List dataLocation) {
        FBI.orderValuesFromFirestore(collection, dataLocation);
    }

    public void updateGlobalScore(int score){
       FBI.updateGlobalScore(score);
    }
}
