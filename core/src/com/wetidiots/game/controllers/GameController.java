package com.wetidiots.game.controllers;

import com.wetidiots.game.FirebaseInterface;
import com.wetidiots.game.models.GamePlay;
import com.wetidiots.game.views.JoinView;
import com.wetidiots.game.models.Question;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

// Connect game methods with methods to read/write from database/firestore
public class GameController {

    private FirebaseInterface _FBI;
    private GamePlay game;
    private List<String> spm;
    private List<String> ids;

    public GameController(FirebaseInterface FBI){
        _FBI = FBI;
        game = new GamePlay(_FBI);
        spm = new ArrayList<>();
        ids = new ArrayList<>();
    }

    public GamePlay getGame() {
        return game;
    }
    public List<String> getSpm() {
        return spm;
    }
    public List getPlayers(){
        return ids;
    }

    public String getUsernamePlayer(int player){
        return (String) game.getPlayers().get(player).get("username");
    }

    public int getPlayersScore(int playerNumber){
        return game.playerScoreByPlayerNumber(playerNumber);
    }

    public List getGameScore(){
        return game.getGameScores();
    }

    public int getPlayerBoxes(int playerNumber){
        return game.playerBoxesByPlayerNumber(playerNumber);
    }

    public void createGame(){
        _FBI.createGame(game);
    }

    public void startGame() {
        game.startGame(_FBI);
    }

    // Clear all variables in the controller
    public void clearGameController(){
        game.clearGame();
        spm.clear();
        ids.clear();
    }

    public void deleteGame(){
        _FBI.deleteGame(game);
    }

    // Method for getting next question for creator
    public Question getNextQuestion() {
        return game.newQuestion();
    }

    // Method for getting next question for joiners
    public Question getNextNewQuestion() {
        return game.nextNewQuestion();
    }

    public void moveToNextQ() {
        game.moveToNextQ(_FBI);
    }

    public void correctAnswer(String username, int score){
        game.updatePlayer(username, "rights", score);
        _FBI.updatePlayer(game, game.findCorrectHashMap(username));
    }

    public void incorrectAnswer(String username){
        game.updatePlayer(username, "wrongs", 0);
        _FBI.updatePlayer(game, game.findCorrectHashMap(username));
    }

    public void deletePlayer(int playerNumber, String gamePin){
        _FBI.deletePlayer(playerNumber, gamePin);
    }

    public void joinGame(String gamePin, HashMap player, JoinView view){
        game.wipePlayers();
        if(game.isReady() == false){
            _FBI.joinGame(gamePin, player, view);
        }
    }

    public void isGameActive(String gamePin){
        _FBI.isGameActive(game, gamePin);
    }

    public void setReady(boolean ready){
        game.setReady(ready);
        _FBI.setReady(game.getGamePin(), ready);
    }

    // Creates a String from from list of all players usernames
    public String getPlayersIDs(){
         _FBI.getPlayers(game, ids);
         String result = "";
         for (String p: ids){
             result += p;
             result += "\n";
         }
         return result;
    }

    public void readToGamePlay(){
        _FBI.readToGamePlay(game);
    }

    public void updateQuestions(List<String> currentQ, List<String> nextQ){
        _FBI.updateQuestions(game, currentQ, nextQ);
    }
}
