package com.wetidiots.game.sprites;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class Avatar extends Sprite{

    private boolean isDead;
    private String picture;

    // Creating avatar with given picture and x-position
    public Avatar(String picture, int x){
        super();
        this.picture = picture;
        this.setTexture(new Texture(Gdx.files.internal("avatars/"+picture+".png")));
        this.setRegion(new TextureRegion(getTexture()), x, 50,60,60);
        this.setPosition(x,50);
        this.isDead = false;
    }

    public void draw(SpriteBatch sb){
        super.draw(sb);
        sb.draw(getTexture(),getX(),getY(),60,60);
    }

    public void die(){
        if (!isDead) {
            this.isDead = true;
            this.setTexture(new Texture(Gdx.files.internal("avatars/"+picture+"_dead.png")));
        }
    }

    public boolean isDead(){
        return isDead;
    }

    public void updatePos(){
        this.setY(getY()+10);
    }
}
