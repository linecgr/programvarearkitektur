package com.wetidiots.game.sprites;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Vector2;
import com.wetidiots.game.Game;

public class Water {

    private Texture texture;
    private Vector2 pos;
    private int timePassed;

    public Water(float x, float y){
        this.texture = new Texture("water1.png");
        this.pos = new Vector2(x,y);
        timePassed = 0;
    }

    public void updatePos(float y){
        if (getPos().y+Game.HEIGHT >= Game.HEIGHT/2f-50){
            this.pos.y = -Game.HEIGHT/2f-50;
        }
        else{
            this.pos.y += (float)Math.pow(1.15,timePassed);
        }
    }

    public void timeHasPassed() {
        this.timePassed ++;
    }

    public Vector2 getPos() {
        return pos;
    }

    public Texture getTexture() {
        return texture;
    }

    public void dispose(){
        this.texture.dispose();
    }
}
