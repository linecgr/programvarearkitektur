package com.wetidiots.game.sprites;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class Box extends Sprite {

    public Box(int x, int y){
        super();
        this.setTexture(new Texture(Gdx.files.internal("box.png")));
        this.setRegion(new TextureRegion(getTexture()), 0, 0,60,10);
        this.setPosition(x,y);
    }

    public void draw(SpriteBatch sb){
        super.draw(sb);
        sb.draw(getTexture(),getX(),getY(),60,10);
    }

    public void dispose(){
        getTexture().dispose();
    }
}
