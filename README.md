# Programvarearkitektur
Wet Idiots is a multiplayer quiz game with fun illustrations making the game more entertaining and interactive.

Press enter game pin to play with friends. Create a game pin and send it to your friends to let them join in on your game. 

Whilst playing, each player will recieve their own avatar standing on a pile in the water. As the players choose incorrect 
answers for the questions, the water rises. When the water completely covers your avatar, you have lost the game of Wet Idiots. 

Press the leaderboard button to see global scores for all games. 

**Here is a step-by-step instruction on how to install, compile and run the game**

1. Clone the project from Gitlab.
2. Open the application in Android Studio.
3. Use the branch named main
4. Choose File in menu followed by Sync Project with Gradle Files
5. Use android as Configuration
6. Use Pixel 5 API 30
7. Choose Run i the menu followed by Run ’android’
8. Use Device manager to view the Android Device
9. The game will pop up by itself

**Here is a step-by-step instruction on how to play the game:**

1. Register a user.
2. Login with email address and password.
3. Click Create a game to create a game.
4. Click Join a game to join a game.
5. Your avatar will appear on the screen along with questions with answer options.
6. Click the answer you think is correct before the time runs out.
7. If the water reaches the top of your avatar, you have lost and the game is over when all
players have lost.
8. When the game is over, a button with the label Go To Leaderboard appears. Click on this
button to see the results from the game.
9. Click on the arrow in the top right corner to go back to the main menu.

**Here is a short description of our folder structure:**

Android module contains:
- **src/com.wetidiots.game** with AndroidInterfaceClass and AndroidLauncher that launches the app
- **assets** for styling

Core module contains:
- **src/com.wetidiots.game/controllers** with all the controllers
- **src/com.wetidiots.game/models** with all the models
- **src/com.wetidiots.game/sprites** with all the sprites
- **src/com.wetidiots.game/views**  with all the views and the abstract class View
- **src/com.wetidiots.game** also contains FirebaseInterface and Game
